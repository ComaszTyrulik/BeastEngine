#pragma once
#include <glm/glm.hpp>

namespace be
{
    using Vec2 = glm::vec2;
    using IntVec2 = glm::ivec2;
} // namespace be
